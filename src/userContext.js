import { createContext } from "react";

// createContext is a function in the React Library that creates a new context object.
// ContextObject is a data type of object that can be used to store information and can be shared to other components within the app.

const UserContext = createContext()

// UserContext.Provider is a component that is created when you use createContext()
// Provider component that allows other components to use the context object and or supply necessary information needed to the context object
export const UserProvider = UserContext.Provider

export default UserContext